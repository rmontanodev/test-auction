<?php

namespace Tests\Util;

use App\Entity\User;
use App\Service\AuctionService;
use App\Entity\Product;
use App\Entity\WinnerUser;
use App\Entity\WinningPrice;
use App\Repository\AuctionRepository;



use App\Tests\Util\Bid as ProductionBid;


it('performs stress test for finding winner', function () {
    $product = AuctionStressTest::testAuctionStress();

    $auctionRepositoryMock = $this->createMock(AuctionRepository::class);

    $auctionRepositoryMock->expects($this->once())
        ->method('save');

    $auctionService = new AuctionService($auctionRepositoryMock);

    $result = $auctionService->findWinner($product, 100);
    expect($result['winner'])->toBeInstanceOf(WinnerUser::class);
    expect($result['winning_price'])->toBeGreaterThan(100);
});

it('performs stress test for saving auction', function () {
    $product = new Product();
    $user = new User();
    // Simulate high load by adding a large number of bids
    for ($i = 0; $i < 1000; $i++) {
        $product->addBid(new ProductionBid(100 + $i,$user));
    }

    $winnerUser = new WinnerUser($user);
    $winningPrice = new WinningPrice(150);
    $auction = new \App\Entity\Auction($product, $winnerUser, $product->getBids(), $winningPrice);

    $auctionRepositoryMock = $this->createMock(AuctionRepository::class);

    $auctionRepositoryMock->expects($this->once())
        ->method('save')
        ->with($auction);

    $auctionService = new AuctionService($auctionRepositoryMock);

    $auctionService->saveAuction($auction);
});
