<?php


namespace Tests\Util;


use App\Entity\Product;
use App\Entity\User;
use App\Service\AuctionService;
use Doctrine\Common\Collections\ArrayCollection;
use App\Tests\Util\Bid;

class AuctionStressTest extends AuctionService
{
    public static function testAuctionStress(): Product
    {
        $users = [];
        for ($i = 0; $i < 500; $i++) {
            $users[] = new User();
        }

        $product = new Product();
        for ($i = 0; $i < 10000; $i++) {
            $user = $users[$i % 500]; // Select a user from list
            $bidAmount = rand(1, 1000); // random bid between 1 and 1000
            $bid = new Bid($bidAmount,$user);
            $product->addBid($bid);
        }
        return $product;
    }
}