<?php
namespace App\Tests\Util;

use App\Entity\Bid as ProductionBid;
use App\Entity\User;

class Bid extends ProductionBid
{
    public function __construct(float $amount, User $user = null)
    {
        if($user !== null){
            $this->setUser($user);
        }
        $this->setAmount($amount);
    }

}