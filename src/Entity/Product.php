<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ORM\Table(name: "Product")]
#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "string", length: 255)]
    private string $name;

    #[ORM\Column(type: "integer")]
    private ?int $reservePrice;

    #[ORM\OneToMany(targetEntity: "App\Entity\Bid", mappedBy: "product", cascade: ["persist"])]
    private Collection $bids;

    #[ORM\OneToMany(targetEntity: "App\Entity\User", mappedBy: "product", cascade: ["persist"])]
    private User $user;


    public function __construct()
    {
        $this->bids = new ArrayCollection();
        $this->name = "Product";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBids(): Collection
    {
        return $this->bids;
    }

    public function addBid(Bid $bid): self
    {
        if (!$this->bids->contains($bid)) {
            $this->bids[] = $bid;
            $bid->setProduct($this);
        }

        return $this;
    }

    public function removeBid(Bid $bid): self
    {
        if ($this->bids->removeElement($bid)) {
            // set the owning side to null (unless already changed)
            if ($bid->getProduct() === $this) {
                $bid->setProduct(null);
            }
        }

        return $this;
    }

    public function getReservePrice(): ?int
    {
        return $this->reservePrice;
    }

    public function setReservePrice(int $reservePrice): self
    {
        $this->reservePrice = $reservePrice;

        return $this;
    }

}
