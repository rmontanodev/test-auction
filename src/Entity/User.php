<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ORM\Table(name: "User")]
#[ORM\Entity(repositoryClass: "App\Repository\UserRepository")]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private $id;

    #[ORM\Column(length: 255)]
    private string $username;

    #[ORM\OneToMany(targetEntity: "App\Entity\Product", mappedBy: "user")]
    private $products;

    #[ORM\OneToMany(targetEntity: "App\Entity\Bid", mappedBy: "user")]
    private $bids;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->bids = new ArrayCollection();
        $this->username = "Username".$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setUser($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getUser() === $this) {
                $product->setUser(null);
            }
        }

        return $this;
    }

    public function getBids(): Collection
    {
        return $this->bids;
    }
}
