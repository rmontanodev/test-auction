<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\BidRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "Bid")]
#[ORM\Entity(repositoryClass: BidRepository::class)]
class Bid
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private $id;

    #[ORM\Column(type: "float")]
    private $amount;

    #[ORM\ManyToOne(targetEntity: "App\Entity\User", inversedBy: "bids")]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\ManyToOne(targetEntity: "App\Entity\Product", inversedBy: "bids")]
    #[ORM\JoinColumn(nullable: false)]
    private $product;

    #[ORM\ManyToOne(targetEntity: "App\Entity\Auction", inversedBy: "bids")]
    #[ORM\JoinColumn(nullable: false)]
    private $auction;


    public function getId(): int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function setAuction(Auction $auction)
    {
        $this->auction = $auction;
    }

    public function getAuction(): Auction
    {
        return $this->auction;
    }


    public static function createBid(Product $product, float $amount): Bid
    {
        return new self($amount);
    }
}
