<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AuctionRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "Auction")]
#[ORM\Entity(repositoryClass: AuctionRepository::class)]
class Auction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Product $product;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $winner;

    #[ORM\OneToMany(targetEntity:  Bid::class, mappedBy: "auction")]
    #[ORM\JoinColumn(nullable: false)]
    private Collection $bids;


    #[ORM\Column(type: "integer")]
    private ?int $winningPrice;

    /**
     * Auction constructor.
     *
     * @param Product $product
     * @param User|null $winner
     * @param Collection $bids
     * @param int|null $winningPrice
     */
    public function __construct(
        Product $product,
        ?User $winner = null,
        Collection $bids = null,
        ?int $winningPrice = null
    ) {
        $this->product = $product;
        $this->winner = $winner;
        $this->bids = $bids ?: new ArrayCollection();
        $this->winningPrice = $winningPrice;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): Auction
    {
        $this->product = $product;
        return $this;
    }

    public function getBids(): Collection
    {
        return $this->bids;
    }

    public function addBid(Bid $bid): Auction
    {
        $this->bids->add($bid);
        $bid->setAuction($this);
        return $this;
    }

    public function removeBid(Bid $bid): Auction
    {
        $this->bids->removeElement($bid);
        if ($bid->getAuction() === $this) {
            $bid->setAuction(null);
        }
        return $this;
    }

    public function getWinningPrice(): ?WinningPrice
    {
        return $this->winningPrice;
    }

    public function setWinningPrice(?WinningPrice $winningPrice): Auction
    {
        $this->winningPrice = $winningPrice;
        if ($winningPrice !== null) {
            $winningPrice->setAuction($this);
        }
        return $this;
    }

    public function getWinner(): ?User
    {
        return $this->winner;
    }

    public function setWinner(User $winner): self
    {
        $this->winner = $winner;
        return $this;
    }

}
