<?php


// src/Service/AuctionService.php

namespace App\Service;

use App\Entity\Auction;
use App\Entity\Bid;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\AuctionRepository;
use Doctrine\Common\Collections\Collection;

class AuctionService
{
    private AuctionRepository $auctionRepository;
    public function __construct(AuctionRepository $auctionRepository) {
        $this->auctionRepository = $auctionRepository;
    }

    public function findWinner(Product $product, float $reservePrice): array
    {
        // 1. Get Bids
        $bids = $product->getBids();

        // 2. Get Valid Bids
        $validBids = $this->getValidBids($bids,$reservePrice);
        $validBidsArray = $validBids->toArray();

        //order desc
        usort($validBidsArray, function ($a, $b) {
            return $b->getAmount() - $a->getAmount();
        });

        $winner = null;

        //set reservePrice min.
        $winning_price = $reservePrice;
        if (count($validBidsArray) > 0) {
            $winner = $this->getWinner($validBids);
            $winning_price = $this->getWinningPrice($validBids,$winner,$reservePrice);

            $auction = new Auction($product,$winner,$bids,$winning_price);
            $this->saveAuction($auction);
        }



        return [
            'winner' => $winner,
            'winning_price' => $winning_price
        ];
    }

    private function getValidBids(Collection $bids,int $reservePrice): Collection
    {
        return $bids->filter(function (Bid $bid) use ($reservePrice) {
            return $bid->getAmount() >= $reservePrice;
        });


    }

    private function getWinner(Collection $bids): User{
        return $bids[0]->getUser();
    }
    private function getWinningPrice(Collection $bids,User $winner,int $reservePrice): int{
        $found = false;
        $i = 1;
        //Set winning price equal to reservePrice
        $winningPrice = $reservePrice;
        do {
            if($bids[$i]->getUser() != $winner){
                //First bid from non-winner
                $winningPrice = $bids[$i]->getAmount();
                $found = true;
            }
            $i++;
        }while(!$found);
        return $winningPrice;
    }
    public function saveAuction(Auction $auction): Auction {
        return $this->auctionRepository->save($auction);
    }
}
