<?php


namespace App\Repository;


use App\Entity\Auction;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AuctionRepository extends ServiceEntityRepository
{
    private ManagerRegistry $registry;

    /**
     * @method Auction|null find($id, $lockMode = null, $lockVersion = null)
     * @method Auction|null findOneBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     * @method Auction[]    findAll()
     * @method Auction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Auction::class);
    }

    public function save(Auction $auction): Auction {
        $entityManager = $this->registry->getEntityManager();
        $entityManager->persist($auction);
        $entityManager->flush();
        return $auction;
    }
}