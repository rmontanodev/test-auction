<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    /**
     * @method Product|null find($id, $lockMode = null, $lockVersion = null)
     * @method Product|null findOneBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     * @method Product[]    findAll()
     * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findWinner(Product $product, float $reservePrice): array
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder
            ->select('b')
            ->from('App\Entity\Bid', 'b')
            ->andWhere('b.product = :product')
            ->andWhere('b.amount >= :reservePrice')
            ->setParameter('product', $product)
            ->setParameter('reservePrice', $reservePrice)
            ->orderBy('b.amount', 'DESC');

        $validBids = $queryBuilder->getQuery()->getResult();

        $winner = null;
        $winningPrice = $reservePrice;

        if (!empty($validBids)) {
            $winner = $validBids[0]->getUser();

            $nonWinnerBid = null;
            foreach ($validBids as $bid) {
                if ($bid->getUser() !== $winner) {
                    $nonWinnerBid = $bid;
                    break;
                }
            }

            if ($nonWinnerBid !== null) {
                $winningPrice = $nonWinnerBid->getAmount();
            }

            return [
                'winner' => $winner,
                'winning_price' => $winningPrice
            ];
        }
    }
}