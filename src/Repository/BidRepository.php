<?php

namespace App\Repository;

use App\Entity\Bid;
use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class BidRepository extends ServiceEntityRepository
{
    private ManagerRegistry $managerRegistry;

    /**
     * @method Product|null find($id, $lockMode = null, $lockVersion = null)
     * @method Product|null findOneBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     * @method Product[]    findAll()
     * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
     */
    public function __construct(ManagerRegistry $managerRegistry) {
        $this->managerRegistry = $managerRegistry;
    }

    public function save(Bid $bid): Bid {
        $entityManager = $this->managerRegistry->getEntityManager();
        $entityManager->persist($bid);
        $entityManager->flush();
        return $bid;
    }
}
