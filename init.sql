CREATE DATABASE IF NOT EXISTS symfony_db;
CREATE DATABASE IF NOT EXISTS symfony_db_test;

GRANT ALL PRIVILEGES ON symfony_db.* TO 'symfony-user'@'%' IDENTIFIED BY 'symfony-password';
GRANT ALL PRIVILEGES ON symfony_db_test.* TO 'symfony-user'@'%' IDENTIFIED BY 'symfony-password';

FLUSH PRIVILEGES;
