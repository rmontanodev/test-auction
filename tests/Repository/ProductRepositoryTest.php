<?php

namespace App\Tests\Repository;

use App\Entity\Bid;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    public function testFindWinner(): void
    {
        $product = new Product();
        $user = new User();

        $bid1 = new Bid();
        $bid1->setAmount(100);
        $bid1->setUser($user);
        $bid1->setProduct($product);

        $bid2 = new Bid();
        $user2 = new User();
        $bid2->setAmount(150);
        $bid2->setUser($user2);
        $bid2->setProduct($product);

        $entityManager = $this->createMock(ManagerRegistry::class);
        $entityManager
            ->method('getRepository')
            ->willReturn($this->createMockRepository([$bid1, $bid2]));

        $result =  $entityManager->getRepository(Product::class)->findWinner($product, 100);

        $this->assertEquals(['winner' => $user2, 'winning_price' => 100], $result);
    }

    private function createMockRepository(array $result): object
    {
        $repository = $this->getMockBuilder(ProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repository
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->willReturn($this->createMockQueryBuilder($result));

        return $repository;
    }

    private function createMockQueryBuilder(array $result): object
    {
        $queryBuilder = $this->getMockBuilder('Doctrine\ORM\QueryBuilder')
            ->disableOriginalConstructor()
            ->getMock();

        $queryBuilder
            ->expects($this->any())
            ->method('select')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('from')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('andWhere')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('setParameter')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('orderBy')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('getQuery')
            ->willReturnSelf();

        $queryBuilder
            ->expects($this->any())
            ->method('getResult')
            ->willReturn($result);

        return $queryBuilder;
    }
}
