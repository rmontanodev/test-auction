<?php

use App\Entity\Product;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    /**
     * @Inject
     */
    private EntityManagerInterface $entityManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
    }
    /**
    @covers User::getId
     */
    public function testGetId()
    {
        $user = new User();

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $id = $user->getId();
        $this->assertNotNull($id);
    }

    /**
    @covers User::getUsername
     */
    public function testGetUsername()
    {
        $user = new User();
        $this->assertIsString($user->getUsername());

    }

    /**
    @covers User::setUsername
     */
    public function testSetUsername(): void
    {
        // Arrange
        $user = new User();
        $username = 'test_username';
        $user->setUsername($username);
        // Assert
        $this->assertTrue( $user->getUsername() == $username);

    }

    /**
    @covers Entity\User::getBids
     */
    public function testGetBids()
    {
        $user = new User();

        $bids = $user->getBids();

        $this->assertInstanceOf(Collection::class, $bids);
    }

    /**
    @covers User::getProducts
     */
    public function testGetProducts()
    {
        $user = new User();

        $products = $user->getProducts();

        $this->assertInstanceOf(Collection::class, $products);
    }

    /**
    @covers User::RemoveProduct
     */
    public function testRemoveProduct()
    {
        $user = new User();
        $product = new Product();

        $user->addProduct($product);

        $user->removeProduct($product);

        $this->assertFalse($user->getProducts()->contains($product));
    }
    /**
    @covers User::AddProduct
     */
    public function testAddProduct(): void
    {
        // Arrange
        $user = new User();
        $product1 = new Product();
        $product2 = new Product();

        // Act
        $user->addProduct($product1);

        // Assert
        $this->assertTrue($user->getProducts()->contains($product1));
        $this->assertSame($user, $product1->getUser());

        // Act
        $user->addProduct($product2);

        // Assert
        $this->assertTrue($user->getProducts()->contains($product2));
        $this->assertSame($user, $product2->getUser());
    }

}
