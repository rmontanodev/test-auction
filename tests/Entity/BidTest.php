<?php
use App\Entity\Bid;
use App\Entity\User;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BidTest extends KernelTestCase
{
    /**
     * @Inject
     */
    private EntityManagerInterface $entityManager;
    private User $testUser;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
        $this->testUser = new User();
    }

    /**
    @covers Bid::getId
     */
    public function testGetId()
    {
        $bid = new Bid();
        $bid->setAmount(100);
        $bid->setUser($this->testUser);
        $this->entityManager->persist($bid);
        $this->entityManager->flush();
        $this->assertNotNull($bid->getId());
    }

    /**
    @covers Bid::GetAmount
     */
    public function testGetAmount(): void
    {
        // Arrange
        $bid = new Bid();
        $bid->setAmount(100.0);

        // Act
        $result = $bid->getAmount();

        // Assert
        $this->assertEquals(100.0, $result);
    }
    /**

    @covers Bid::SetAmount
     */
    public function testSetAmount(): void
    {
        // Arrange
        $bid = new Bid();

        // Act
        $bid->setAmount(150.0);
        $result = $bid->getAmount();

        // Assert
        $this->assertEquals(150.0, $result);
    }
    /**

    @covers Bid::GetUser
     */
    public function testGetUser(): void
    {
        // Arrange
        $bid = new Bid();
        $user = new User();
        $bid->setUser($user);

        // Act
        $result = $bid->getUser();

        // Assert
        $this->assertSame($user, $result);
    }
    /**

    @covers Bid::SetUser
     */
    public function testSetUser(): void
    {
        // Arrange
        $bid = new Bid();
        $user = new User();

        // Act
        $bid->setUser($user);
        $result = $bid->getUser();

        // Assert
        $this->assertSame($user, $result);
    }
    /**
    @covers Bid::GetProduct
     */
    public function testGetProduct(): void
    {
        // Arrange
        $bid = new Bid();
        $product = new Product();
        $bid->setProduct($product);

        // Act
        $result = $bid->getProduct();

        // Assert
        $this->assertSame($product, $result);
    }
    /**

     * @covers Bid::SetProduct
     */
    public function testSetProduct(): void
    {
        // Arrange
        $bid = new Bid();
        $product = new Product();

        // Act
        $bid->setProduct($product);
        $result = $bid->getProduct();

        // Assert
        $this->assertSame($product, $result);
    }
}