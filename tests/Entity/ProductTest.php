<?php

namespace App\Tests\Entity;

use App\Entity\Bid;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductTest extends KernelTestCase
{
    /**
     * @Inject
     */
    private EntityManagerInterface $entityManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::bootKernel()->getContainer()->get('doctrine.orm.entity_manager');
    }
    /**
    @covers Product::setName
    @covers Product::getName
     */
    public function testNameCanBeSetAndGet()
    {
        $product = new Product();
        $product->setName('Test Product');
        $this->assertEquals('Test Product', $product->getName());
    }

    /**
    @covers Product::setReservePrice
    @covers Product::getReservePrice
     */
    public function testReservePriceCanBeSetAndGet()
    {
        $product = new Product();
        $product->setReservePrice(100);
        $this->assertEquals(100, $product->getReservePrice());
    }

    /**
    @covers Product::setUser
    @covers Product::getUser
     */
    public function testUserCanBeSetAndGet()
    {
        $product = new Product();
        $user = $this->createMock(User::class);
        $product->setUser($user);
        $this->assertEquals($user, $product->getUser());
    }

    /**
    @covers Product::addBid
    @covers Product::getBids
     */
    public function testBidsCanBeAddedAndRemoved()
    {
        $product = new Product();
        $this->assertCount(0, $product->getBids());

        $bid1 = $this->createMock(Bid::class);
        $product->addBid($bid1);
        $this->assertCount(1, $product->getBids());
        $this->assertTrue($product->getBids()->contains($bid1));

        $bid2 = $this->createMock(Bid::class);
        $product->addBid($bid2);
        $this->assertCount(2, $product->getBids());
        $this->assertTrue($product->getBids()->contains($bid2));

        $product->removeBid($bid1);
        $this->assertCount(1, $product->getBids());
        $this->assertFalse($product->getBids()->contains($bid1));
        $product->removeBid($bid2);
        $this->assertCount(0, $product->getBids());

    }

    /**
     * @covers Product::getId
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testGetIdReturnsNullInitially()
    {
        $product =  $this->createMock(Product::class);
        $this->assertNull($product->getId());
    }

    /**
    @covers Product::getId
     */
    public function testGetId()
    {
        $product = new Product();
        $product->setReservePrice(100);
        $this->entityManager->persist($product);
        $this->entityManager->flush();
        $this->assertNotNull($product->getId());
    }
}
