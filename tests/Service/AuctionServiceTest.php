<?php

use App\Entity\Auction;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use App\Service\AuctionService;
use App\Entity\Product;
use App\Tests\Util\Bid;
use App\Repository\AuctionRepository;
use Doctrine\Common\Collections\ArrayCollection;

class AuctionServiceTest extends TestCase
{
    public function testFindWinnerNoBids()
    {
        // Arrange
        $product = new Product();
        $reservePrice = 100;
        $auctionRepositoryMock = $this->createMock(AuctionRepository::class);
        $auctionService = new AuctionService($auctionRepositoryMock);

        // Act
        $result = $auctionService->findWinner($product, $reservePrice);

        // Assert
        $this->assertNull($result['winner']);
        $this->assertEquals($reservePrice, $result['winning_price']->getAmount());
    }

    public function testFindWinnerWithBids()
    {
        // Arrange
        $user1 = new User();
        $user2 = new User();
        $user3 = new User();

        $bids = new ArrayCollection([
            new Bid(150, $user1),
            new Bid(200, $user2),
            new Bid(120, $user3),
            new Bid(180, $user1)
        ]);

        $product = new Product();
        foreach ($bids as $bid){
            $product->addBid($bid);
        }
        $reservePrice = 100;

        $auctionRepositoryMock = $this->createMock(AuctionRepository::class);
        $auctionRepositoryMock->expects($this->once())
            ->method('save');

        $auctionService = new AuctionService($auctionRepositoryMock);

        // Act
        $result = $auctionService->findWinner($product, $reservePrice);

        // Assert
        $this->assertEquals($user2, $result['winner']);
        $this->assertEquals(180, $result['winning_price']->getAmount());
    }

    public function testSaveAuction()
    {
        $user1 = new User();
        $product = new Product();
        $bids = new ArrayCollection(  [new Bid(150, $user1)]);
        foreach ($bids as $bid){
            $product->addBid($bid);
        }
        $winningPrice = 150;
        // Arrange
        $auction = new Auction($product,$user1,$bids,$winningPrice );

        $auctionRepositoryMock = $this->createMock(AuctionRepository::class);
        $auctionRepositoryMock->expects($this->once())
            ->method('save')
            ->with($auction);

        $auctionService = new AuctionService($auctionRepositoryMock);

        $auction = $auctionService->saveAuction($auction);
        $this->assertEquals(150, $auction->getWinningPrice());
        $this->assertEquals($product, $auction->getProduct());
        $this->assertEquals($user1, $auction->getWinner());
        $this->assertEquals($bids, $auction->getProduct()->getBids());

    }
}
