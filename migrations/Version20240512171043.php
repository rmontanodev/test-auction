<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240512171043 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Auction (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, winner_id INT DEFAULT NULL, winning_price INT NOT NULL, INDEX IDX_1159CC0F4584665A (product_id), INDEX IDX_1159CC0F5DFCD4B8 (winner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Auction ADD CONSTRAINT FK_1159CC0F4584665A FOREIGN KEY (product_id) REFERENCES Product (id)');
        $this->addSql('ALTER TABLE Auction ADD CONSTRAINT FK_1159CC0F5DFCD4B8 FOREIGN KEY (winner_id) REFERENCES User (id)');
        $this->addSql('ALTER TABLE Bid ADD auction_id INT NOT NULL');
        $this->addSql('ALTER TABLE Bid ADD CONSTRAINT FK_72BFF51357B8F0DE FOREIGN KEY (auction_id) REFERENCES Auction (id)');
        $this->addSql('CREATE INDEX IDX_72BFF51357B8F0DE ON Bid (auction_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Bid DROP FOREIGN KEY FK_72BFF51357B8F0DE');
        $this->addSql('ALTER TABLE Auction DROP FOREIGN KEY FK_1159CC0F4584665A');
        $this->addSql('ALTER TABLE Auction DROP FOREIGN KEY FK_1159CC0F5DFCD4B8');
        $this->addSql('DROP TABLE Auction');
        $this->addSql('DROP INDEX IDX_72BFF51357B8F0DE ON Bid');
        $this->addSql('ALTER TABLE Bid DROP auction_id');
    }
}
