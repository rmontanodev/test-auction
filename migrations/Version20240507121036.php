<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240507121036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Product DROP FOREIGN KEY FK_1CF73D31A76ED395');
        $this->addSql('DROP INDEX IDX_1CF73D31A76ED395 ON Product');
        $this->addSql('ALTER TABLE Product DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Product ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE Product ADD CONSTRAINT FK_1CF73D31A76ED395 FOREIGN KEY (user_id) REFERENCES User (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1CF73D31A76ED395 ON Product (user_id)');
    }
}
