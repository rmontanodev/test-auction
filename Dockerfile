# Use an official PHP runtime as a parent image
FROM php:8.1-fpm

COPY init.sql /docker-entrypoint-initdb.d/

# Set the working directory to /app
WORKDIR /app

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    unzip \
    libicu-dev \
    libpq-dev \
    libzip-dev \
    gnupg2

# Install PHP extensions
RUN docker-php-ext-install pdo pdo_pgsql pdo_mysql intl zip

# Install Xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

# Configure Xdebug
RUN echo "xdebug.mode=coverage " >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy the application code into the container
COPY .. /app

COPY ./data /var/lib/postgresql/data
RUN groupadd postgres
RUN useradd -g postgres postgres
RUN chown -R postgres:postgres /var/lib/postgresql/data
RUN chmod -R 770 /app/data
RUN chmod -R 770 /var/lib/postgresql/data

# Install PHP dependencies
RUN \
    COMPOSER_ALLOW_SUPERUSER=1 composer install

# Expose port 9000 and start PHP-FPM server
EXPOSE 9000
CMD ["php-fpm"]
